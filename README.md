# post-cross

Le projet consiste à rendre disponible une plateforme web permettant à un utilisateur de créer un article et de publier celui-ci sur ses différents réseaux sociaux.
Elle doit pouvoir gérer plusieurs utilisateurs avec différents rôles et pouvoir gérer les publications qui seront envoyées aux réseaux sociaux.

# Pré-requis

Il faut avoir sur son poste le nécéssaire pour éxécuter le projet c'est à dire :

* Open JDK 8 (pour le développement avec Java)
* Maven (Le compilateur que nous utilisons)
* PostgreSQL ( notre base de données )
* PgAdmin4 ( C'est une interface UI web pour gérer postgres )

Il faut bien vérifier les PATH : JAVA_HOME et MAVEN_HOME, sinon, nous ne pourrons pas utiliser java ou maven.

Pour la base de données du projet, il faut en créer une avec le nom `postcross`, un utilisateur `postcross` et un mot de passe `postcross`

# Dépendances du projet

* Spring Web Starter : Nous aide à créer une application basée sur le web

* Spring Data JPA : pour fournir l'API d'accès aux données que Hibernate utilisera

* H2 Database : permet de charger une base de données temporaire pour les tests dans notre proejt

* PostgreSQL : nous permet de se connecter à une base de données PostgreSQL

* Spring Core, Config, Social est une extension de Spring Framework, elle permet de connecter les applications à des fournisseurs d'API Software-as-a-Service (SaaS) tels que Facebook, Twitter et LinkedIn.

* Spring Security : est une infrastructure Java / Java EE qui fournit des fonctionnalités d'authentification, d'autorisation et de sécurité pour les applications d'entreprise



# Exécution du projet
Depuis un terminal ou sur l'IDE.

Nettoyer et installer les dépendances du projet :  
```
mvn clean install
```
Exécuter le projet sur le serveur web en utilisant le compilateur et le plugin de spring :  

```
mvn spring-boot:run
```

Si vous n'avez pas d'erreur dans la compilation et que vous voyez un message sans erreur de la part de Tocat c'est que le serveur web fonctionne :
```
2019-12-13 15:48:53.032  INFO 6899 --- [  restartedMain] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2019-12-13 15:48:53.032  INFO 6899 --- [  restartedMain] org.apache.catalina.core.StandardEngine  : Starting Servlet Engine: Apache Tomcat/8.5.15
```

A la fin vous devez avoir un message de Spring Security pour dire que l'application est prête : 
```
2019-12-13 15:48:55.699  INFO 6899 --- [  restartedMain] c.y.p.springsecurity.Application         : Started Application in 4.251 seconds (JVM running for 4.508)
``` 

Vous pouvez vous connecter à la plateforme à l'adresse : `http://localhost:8080`

La documentation API est au format JSON à l'adresse : `http://localhost:8080/v2/api-docs` 
Nous avons manqué de temps pour finir de mettre l'interface UI en place. Elle aurait apporté une valeur ajoutée pour comprendre le fonctionnement de notre API.

La documentation API avec l'interface devrait être sur l'adresse : `localhost:8080/swagger-ui.html`

# Sonar
Nous voulons dans la suite utiliser Sonar pour la qualité de notre logiciel

# Tests
Il y a un seul test pour la compilation de l'application, fait en JUnit.

# IC
Nous n'avons pas utilisé la configuration gitlab-cy.yml pour l'inégration continu avec la création d'une image docker, toute fois cela sera fait par la suite.

# Environement
Aujourd'hui, il y a un profil spring 'DEV' créé dans le repertoire "resources" du projet

# Facebook

Pour configurer les informations de facebook il faut editer le fichier : application.properties et mettre l'`appid` facebook avec le `secret` qui sont dans la partie développeur de facebook sur leur site dédié.

`spring.social.facebook.appId = `
`spring.social.facebook.appSecret = `


## Reference Documentation
Pour les références, considérez les sections suivantes : 

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.1.RELEASE/maven-plugin/)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.2.1.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Thymeleaf](https://docs.spring.io/spring-boot/docs/2.2.1.RELEASE/reference/htmlsingle/#boot-features-spring-mvc-template-engines)

## Guides
Les guides suivants illustrent comment utiliser quelques fonctionnalités : 

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Handling Form Submission](https://spring.io/guides/gs/handling-form-submission/)
