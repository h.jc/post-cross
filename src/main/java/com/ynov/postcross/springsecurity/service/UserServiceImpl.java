package com.ynov.postcross.springsecurity.service;

import com.ynov.postcross.springsecurity.model.Role;
import com.ynov.postcross.springsecurity.model.UserPostcross;
import com.ynov.postcross.springsecurity.repository.RoleRepository;
import com.ynov.postcross.springsecurity.repository.UserRepository;
import com.ynov.postcross.springsecurity.web.dto.UserRegistrationDto;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder passwordEncoder;

    private final RoleRepository roleRepository;

    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    /**
     * Permet de récupérer un utilisateur en cherchant par son adresse email
     * @param email
     * @return
     */
    public UserPostcross findByEmail(String email){
        return userRepository.findByEmail(email);
    }

    /**
     * Le service va sauvegarder un utilisateur
     * @param registration
     * @return
     */
    public UserPostcross save(UserRegistrationDto registration){
        List<Role> role = roleRepository.findAll();

        //TODO Penser à mettre des vrai logs
        System.out.println("----------------------------------------------------------------------");
        System.out.println(" UserServiceImpl : Contenu de l'objet registration :");
        System.out.println(registration);

        UserPostcross user = new UserPostcross();
        user.setFirstName(registration.getFirstName());
        user.setLastName(registration.getLastName());
        user.setEmail(registration.getEmail());
        user.setMdp(passwordEncoder.encode(registration.getPassword()));
        user.setRoles(role);

        //TODO Penser à mettre des vrai logs
        System.out.println("----------------------------------------------------------------------");
        System.out.println(" UserServiceImpl : Contenu de l'objet user :");
        System.out.println(user);

        return userRepository.save(user);
    }

    /**
     * Vérifie si l'email existe déjà
     * @param email
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserPostcross user = userRepository.findByEmail(email);
        if (user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(),
                user.getMdp(),
                mapRolesToAuthorities(user.getRoles()));
    }

    /**
     * Maps les rôles et affecte les autorisations les plus simple.
     * @param roles
     * @return
     */
    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles){
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }
}
