package com.ynov.postcross.springsecurity.service;

import com.ynov.postcross.springsecurity.model.UserPostcross;
import com.ynov.postcross.springsecurity.web.dto.UserRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    /**
     * Fait une recherche de l'existance de l'email dans le service hérité
     * @param email
     * @return
     */
    UserPostcross findByEmail(String email);

    /**
     * Cette méthode permet de sauvegarde
     * @param registration
     * @return
     */
    UserPostcross save(UserRegistrationDto registration);
}
