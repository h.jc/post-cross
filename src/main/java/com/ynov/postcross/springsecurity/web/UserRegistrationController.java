package com.ynov.postcross.springsecurity.web;

import com.ynov.postcross.springsecurity.model.UserPostcross;
import com.ynov.postcross.springsecurity.service.UserServiceImpl;
import com.ynov.postcross.springsecurity.web.dto.UserRegistrationDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/registration")
public class UserRegistrationController {

    @Autowired
    private UserServiceImpl userService;

    @ModelAttribute("user")
    public UserRegistrationDto userRegistrationDto() {
        return new UserRegistrationDto();
    }

    @ApiOperation(value = "Envoir le template du formulaire pour enregistrer un utilisateur")
    @GetMapping
    public String showRegistrationForm(Model model) {
        return "registration";
    }

    @ApiOperation(value = "Reçoit les données du formulaire pour enregistrer un utilisateur")
    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") @Valid UserRegistrationDto userDto,
                                      BindingResult result){

        //TODO penser à mettre des vrai logs
        System.out.println("----------------------------------------------------------------------");
        System.out.println(" CONTROLER : Contenu de l'objet user :");
        System.out.println(userDto);

        // Vérifie l'email de l'utilisateur si il existe déjà
        UserPostcross existing = userService.findByEmail(userDto.getEmail());
        if (existing != null){
            result.rejectValue("email", null, "There is already an account registered with that email");
        }

        // TODO penser à mettre des vrai logs
        // Si il y a une erreur on renvoi le template pour enregistrer un utilisateur
        if (result.hasErrors()){
            System.out.println("-----------------------------------------");
            System.out.println("TRUE : hasErrors");
            return "registration";
        }


        // Sauvegarde de l'utilisateur
        userService.save(userDto);
        return "redirect:/registration?success";
    }

}
