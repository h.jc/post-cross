package com.ynov.postcross.springsecurity.web;

import io.swagger.annotations.ApiOperation;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.social.facebook.api.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * Controleur principal de l'application
 */
@Controller
public class MainController {

    private Facebook facebook;
    private ConnectionRepository connectionRepository;

    public MainController(Facebook facebook, ConnectionRepository connectionRepository) {
        this.facebook = facebook;
        this.connectionRepository = connectionRepository;
    }

    @ApiOperation(value = "Envoi le template de point d'entrée de l'application")
    @GetMapping("/")
    public String root() {

        //TODO C'est pas très propoe pouruoi pas plus tard mettre ça dans une classe de constante pour centralisé l'utilisation des string pour les templates.
        String template = "index";
        return template;
    }

    @ApiOperation(value = "Envoi le template du formulaire d'authentification")
    @GetMapping("/login")
    public String login(Model model) {

        //TODO C'est pas très propoe pouruoi pas plus tard mettre ça dans une classe de constante pour centralisé l'utilisation des string pour les templates.
        String template = "login";
        return template;
    }

    @ApiOperation(value = "Envoi le template de l'espace de l'utilisateur une fois qu'il est authentifié")
    @GetMapping("/user")
    public String userIndex() {

        //TODO C'est pas très propoe pouruoi pas plus tard mettre ça dans une classe de constante pour centralisé l'utilisation des string pour les templates.
        String template = "user/index";
        return template;
    }

    @ApiOperation(value = "Récupère les informations de facebook si l'utlisateur est connecté puis envoi un template avec les infos du mur.")
    @GetMapping("/feed")
    public String feed(Model model) {

        if(connectionRepository.findPrimaryConnection(Facebook.class) == null) {
            return "redirect:/api/facebook";
        }

        //TODO C'est pas très propoe pouruoi pas plus tard mettre ça dans une classe de constante pour centralisé l'utilisation des string pour les templates.
        String template = "feed";

        PagedList<Post> userFeed = facebook.feedOperations().getFeed();
        model.addAttribute("userFeed", userFeed);

        // TODO : finir de mettre des vrai logs dans l'appli.
        System.out.println("------------------------------------------");
        System.out.println("DATA :");
        System.out.println(facebook.feedOperations());

        return template;
    }

    @ApiOperation(value = "Récupère les informations de facebook si l'utlisateur est connecté puis envoi un template avec les infos des amies.")
    @GetMapping("/friends")
    public String friends(Model model) {

        //TODO C'est pas très propoe pouruoi pas plus tard mettre ça dans une classe de constante pour centralisé l'utilisation des string pour les templates.
        String template = "friends";

        if(connectionRepository.findPrimaryConnection(Facebook.class) == null) {
            return "redirect:/api/facebook";
        }

        // TODO : IL y a un soucis avec les objets données par spring social, voir pour les faire nous même.
        // User userProfile = facebook.userOperations().getUserProfile();
        // model.addAttribute("userProfile", userProfile);

        List<User> friends = facebook.friendOperations().getFriendProfiles();
        model.addAttribute("friends", friends);

        return template;
    }

    //Test pour surcharger le service REST connect facebook
    @ApiOperation(value = "Envoi le formulaire pour la connexion à Facebook")
    @GetMapping("/api/facebook")
    public String facebook(Model model) {

        String template = "api/facebook/connect";
        return template;
    }
}
