package com.ynov.postcross.springsecurity.repository;

import com.ynov.postcross.springsecurity.model.UserPostcross;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserPostcross, Long> {
	UserPostcross findByEmail(String email);
}
