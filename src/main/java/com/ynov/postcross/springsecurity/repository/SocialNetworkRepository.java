package com.ynov.postcross.springsecurity.repository;

import com.ynov.postcross.springsecurity.model.SocialNetwork;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface SocialNetworkRepository extends JpaRepository<SocialNetwork, Long> {
}
