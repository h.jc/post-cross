package com.ynov.postcross.springsecurity.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "social_network", schema = "public")
@EntityListeners(AuditingEntityListener.class)
public class SocialNetwork {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "urlSocial", nullable = false)
    private String urlSocial;

    @Column(name = "loginSocial", nullable = false)
    private String loginSocial;

    @Column(name = "passwordSocial", nullable = false)
    private String passwordSocial;

    @Column(name = "token", nullable = false)
    private String token;

    @Column(name = "dateExpire", nullable = false)
    private LocalDate dateExpire;

    @Column(name = "dateCreate", nullable = false)
    private LocalDate dateCreate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlSocial() {
        return urlSocial;
    }

    public void setUrlSocial(String urlSocial) {
        this.urlSocial = urlSocial;
    }

    public String getLoginSocial() {
        return loginSocial;
    }

    public void setLoginSocial(String loginSocial) {
        this.loginSocial = loginSocial;
    }

    public String getPasswordSocial() {
        return passwordSocial;
    }

    public void setPasswordSocial(String passwordSocial) {
        this.passwordSocial = passwordSocial;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDate getDateExpire() {
        return dateExpire;
    }

    public void setDateExpire(LocalDate dateExpire) {
        this.dateExpire = dateExpire;
    }

    public LocalDate getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(LocalDate dateCreate) {
        this.dateCreate = dateCreate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SocialNetwork that = (SocialNetwork) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(urlSocial, that.urlSocial) &&
                Objects.equals(loginSocial, that.loginSocial) &&
                Objects.equals(passwordSocial, that.passwordSocial) &&
                Objects.equals(token, that.token) &&
                Objects.equals(dateExpire, that.dateExpire) &&
                Objects.equals(dateCreate, that.dateCreate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, urlSocial, loginSocial, passwordSocial, token, dateExpire, dateCreate);
    }

    @Override
    public String toString() {
        return "SocialNetwork{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", urlSocial='" + urlSocial + '\'' +
                ", loginSocial='" + loginSocial + '\'' +
                ", passwordSocial='" + passwordSocial + '\'' +
                ", token='" + token + '\'' +
                ", dateExpire=" + dateExpire +
                ", dateCreate=" + dateCreate +
                '}';
    }
}
